import {
  NEW_GAME_SUCCEEDED,
  NEW_GAME_FAILED,
  GUESS_SUCCEEDED,
  GUESS_FAILED,
} from '../../src/actions/index';

import {
  newGame,
  newGuess,
} from '../../src/actions/GameServerActions';

describe('newGame', () => {
  let xhr;
  let requests;
  let dispatch;

  beforeEach(() => {
    // Mock out XMLHttpRequest with sinon
    global.XMLHttpRequest = xhr = sinon.useFakeXMLHttpRequest();

    // Store XMLHttpRequests to respond to them
    requests = [];
    xhr.onCreate = (xhr) => requests.push(xhr);
    dispatch = sinon.stub();
  });

  afterEach(() => {
    xhr.restore();
  });

  it('dispatches new game failed when xhr fails', () => {
    newGame({game_type: 'guess_number'})(dispatch);

    requests[0].respond(503, {}, JSON.stringify({error: 'error'}));

    expect(dispatch).to.have.been.calledWith({
      type: NEW_GAME_FAILED,
      payload: {error: 'error'}
    });
  });

  it('dispatches new game succeeded when xhr succeeds', () => {
    newGame({game_type: 'guess_number'})(dispatch);

    requests[0].respond(201, {}, JSON.stringify({id: 'id',
                                                type: 'guess_number',
                                                status: 'waiting_for_move'}));

    expect(dispatch).to.have.been.calledWith({
      type: NEW_GAME_SUCCEEDED,
      payload: {id: 'id',
                type: 'guess_number',
                status: 'waiting_for_move'}
    });
  });
});

describe('newGuess', () => {
  let xhr;
  let requests;
  let dispatch;

  beforeEach(() => {
    // Mock out XMLHttpRequest with sinon
    global.XMLHttpRequest = xhr = sinon.useFakeXMLHttpRequest();

    // Store XMLHttpRequests to respond to them
    requests = [];
    xhr.onCreate = (xhr) => requests.push(xhr);
    dispatch = sinon.stub();
  });

  afterEach(() => {
    xhr.restore();
  });

  it('dispatches guess failed when xhr fails', () => {
    newGuess({id: 'id', guess: 'guess'})(dispatch);

    requests[0].respond(503, {}, JSON.stringify({error: 'error'}));

    expect(dispatch).to.have.been.calledWith({
      type: GUESS_FAILED,
      payload: {error: 'error'}
    });
  });

  it('dispatches guess succeeded when xhr succeeds', () => {
    newGuess({id: 'id', guess: 1})(dispatch);

    requests[0].respond(201, {},
      JSON.stringify({moveResponse: {move: {comparedToAnswer: 'LT', guess: 1},
                                     game: {id: 'id',
                                            type: 'guess_number',
                                            status: 'waiting_for_move'}}}));

    expect(dispatch).to.have.been.calledWith({
      type: GUESS_SUCCEEDED,
      payload: {moveResponse: {move: {comparedToAnswer: 'LT', guess: 1},
                              game: {id: 'id',
                                    type: 'guess_number',
                                    status: 'waiting_for_move'}}}
    });
  });
});
