// import React from 'react';
// import {shallow} from 'enzyme';
//
// import WordGameComponent from '../../../src/containers/GuessWord/GuessWord';
// import WordGame from '../../../src/games/WordGame';
// import InputChangesOnSubmit from '../../../src/components/InputChangesOnSubmit';
// import MoveHistory from '../../../src/components/WordGame/WordGameHistory';
//
// describe('WordGameComponent', () => {
//   it('renders', () => {
//     expect(shallow(
//       <WordGameComponent game={WordGame.generate()} key='0' />
//     )).to.exist;
//   });
//
//   it('has a title, a paragraph, one InputChangesOnSubmit and MoveHistory', () => {
//     const wordGame = shallow(<WordGameComponent game={WordGame.generate()} key='0' />);
//
//     expect(wordGame).to.contain(<p> Guess a 5 letter word </p>);
//     expect(wordGame).to.contain(<h3> Word Guess Game </h3>);
//
//     expect(wordGame).to.have.exactly(1).descendants(InputChangesOnSubmit);
//   });
//
// });
