// import React from 'react';
// import {shallow} from 'enzyme';
//
// import NumberGameComponent from '../../../src/containers/GuessNumber/GuessNumber';
// import NumberGame from '../../../src/games/NumberGame';
// import InputChangesOnSubmit from '../../../src/components/InputChangesOnSubmit';
// import MoveHistory from '../../../src/components/NumberGame/NumberGameHistory';
//
// describe('NumberGameComponent', () => {
//   it('renders', () => {
//     expect(shallow(
//       <NumberGameComponent game={NumberGame.generate()} key='0' />
//     )).to.exist;
//   });
//
//   it('has a title, a paragraph, one InputChangesOnSubmit and MoveHistory', () => {
//     const numberGame = shallow(<NumberGameComponent game={NumberGame.generate()} key='0' />);
//
//     expect(numberGame).to.contain(<p> Guess a number from 0 to 9 </p>);
//     expect(numberGame).to.contain(<h3> Number Guess Game </h3>);
//
//     expect(numberGame).to.have.exactly(1).descendants(InputChangesOnSubmit);
//     expect(numberGame).to.have.exactly(1).descendants(MoveHistory);
//   });
//
// });
