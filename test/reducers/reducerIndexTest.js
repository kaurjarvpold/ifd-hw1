import reducer from '../../src/reducers/index';
import {
  newGameRequested,
  newGameSucceeded,
  newGameFailed,
  guessRequested,
  guessSucceeded,
  guessFailed,
  connecting,
  connected,
  disconnected
} from '../../src/actions/index';

describe('GameReducer', () => {
  it('has no games or pending request initially', () => {
    expect(reducer(undefined, {}))
    .to.eql({
      connection_status: "disconnected",
      currentPlayer: "",
      games: [],
      gameInFlight: false,
      moveInFlight: false,
      players: []
    });
  });

  it('sets game in flight when a new game is requested', () => {
    expect(
      reducer(undefined, newGameRequested({game_type: 'guess_number'}))
    ).to.eql({
      connection_status: "disconnected",
      currentPlayer: "",
      games: [],
      gameInFlight: true,
      moveInFlight: false,
      players: [],
    });
  });

  it('sets move in flight when a new move is requested', () => {
    expect(
      reducer(undefined, guessRequested({id: 'fsdfsdfsdf', guess: 'paper'}))
    ).to.eql({
      connection_status: "disconnected",
      currentPlayer: "",
      games: [],
      gameInFlight: false,
      moveInFlight: true,
      players: [],
    });
  });

  it('sets connection_status to connecting when connection is requested', () => {
    expect(
      reducer(undefined, connecting({name: 'Peeter'}))
    ).to.eql({
      connection_status: "connecting",
      currentPlayer: "",
      games: [],
      gameInFlight: false,
      moveInFlight: false,
      players: [],
    });
  });

  it('sets connection_status to connected when connection is established', () => {
    expect(
      reducer(undefined, connected())
    ).to.eql({
      connection_status: "connected",
      currentPlayer: "",
      games: [],
      gameInFlight: false,
      moveInFlight: false,
      players: [],
    });
  });

  it('sets state to its initial values when disconnected', () => {
    expect(
      reducer(undefined, disconnected())
    ).to.eql({
      connection_status: "disconnected",
      currentPlayer: "",
      games: [],
      gameInFlight: false,
      moveInFlight: false,
      players: [],
    });
  });

});
