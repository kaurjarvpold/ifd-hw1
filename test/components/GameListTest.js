import React from 'react';
import {shallow, render} from 'enzyme';

import GameList from '../../src/components/GameList';
import NumberGameComponent from '../../src/components/NumberGame/NumberGameComponent';
import WordGameComponent from '../../src/components/WordGame/WordGameComponent';

describe('GameList', () => {
  it('renders no NumberGame components without games', () => {
    expect(shallow(<GameList games={[]} />))
      .to.not.contain.descendants(NumberGameComponent);
  });

  it('renders no WordGame components without games', () => {
    expect(shallow(<GameList games={[]} />))
      .to.not.contain.descendants(WordGameComponent);
  });

  it('renders Game is loading, when a new game request is in flight', () => {
    expect(shallow(<GameList games={[]} gameInFlight={true}/>))
      .to.contain.text('Game is loading...');
  });

  it('does not render Game is loading, when a new game request is not in flight', () => {
    expect(shallow(<GameList games={[]} gameInFlight={false}/>))
      .to.not.contain.text('Game is loading...');
  });

});
