import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';

import DisConnect from '../../src/components/DisConnect';

describe('<DisConnect>', function () {
  it('should have a button', function () {
    const wrapper = shallow(<DisConnect/>);
    expect(wrapper.find('button')).to.have.length(1);
  });

  it('should have props for disconnecting from a websocket', function () {
    const wrapper = shallow(<DisConnect/>);
    expect(wrapper.props().disConnect).to.be.defined;
  });
});
