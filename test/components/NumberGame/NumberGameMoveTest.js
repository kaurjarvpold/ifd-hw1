// import React from 'react';
// import {shallow, render} from 'enzyme';
//
// import Move from '../../../src/components/NumberGame/NumberGameMove';
//
// describe('Number game move', () => {
//
//   it('renders information about the move', () => {
//     expect(shallow(
//       <Move guess='1' comparedToAnswer='EQ'></Move>
//     )).to.include.text('1: was correct');
//   });
//
//   it('sets the right class name depending wether the guess was right or wrong', () => {
//     expect(
//       render(<Move guess='1' comparedToAnswer='EQ'></Move>).html()
//     ).to.eql('<div class="move-correct"><span><i>1</i>: was correct</span></div>');
//   });
// });
