// import React from 'react';
// import {shallow} from 'enzyme';
//
// import MoveHistory from '../../../src/components/NumberGame/NumberGameHistory';
// import Move from '../../../src/components/NumberGame/NumberGameMove';
//
// describe('Number game move history', () => {
//   it('renders no Move components without moves', () => {
//     expect(shallow(<MoveHistory moves={[]} />))
//       .to.not.contain.descendants(Move);
//   });
//
//   it('renders Move component for each move', () => {
//     const moves = [
//       {comparedToAnswer: 'LT', guess: '1'},
//       {comparedToAnswer: 'EQ', guess: '5'}
//     ];
//
//     const moveHistory = shallow(<MoveHistory moves={moves} />);
//
//     expect(moveHistory).to.have.exactly(2).descendants(Move);
//     expect(moveHistory).to.contain(<Move comparedToAnswer="LT" guess='1'/>);
//     expect(moveHistory).to.contain(<Move comparedToAnswer="EQ" guess='5'/>);
//   });
// });
