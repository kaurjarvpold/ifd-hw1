// import React from 'react';
// import {shallow, render} from 'enzyme';
//
// import Char from '../../../src/components/WordGame/WordGameChar';
//
// describe('Word game char', () => {
//
//   it('renders a green character when it is correct', () => {
//     expect(
//       render(<Char a='a' b='a'/>).html()
//     ).to.eql('<span class="Green">a</span>');
//   });
//
//   it('renders a red character when it is incorrect', () => {
//     expect(
//       render(<Char a='a' b='b'/>).html()
//     ).to.eql('<span class="Red">a</span>');
//   });
// });
