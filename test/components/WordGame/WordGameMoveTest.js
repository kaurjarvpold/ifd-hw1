// import React from 'react';
// import {shallow, render} from 'enzyme';
//
// import Move from '../../../src/components/WordGame/WordGameMove';
// import Char from '../../../src/components/WordGame/WordGameChar';
//
// describe('Word game move', () => {
//
//   it('renders Char component for each character of the guess', () => {
//     const move = shallow(<Move guess='paper' comparedToAnswer='EQ' targetWord='paper'/>);
//
//     expect(move).to.have.exactly(5).descendants(Char);
//   });
//
//   it('sets the right class name depending wether the guess was right or wrong', () => {
//     expect(
//       render(<Move guess='1' comparedToAnswer='EQ' targetWord='paper'></Move>).html()
//     ).to.eql('<div class="move correct"><span class="Red">1</span></div>');
//   });
// });
