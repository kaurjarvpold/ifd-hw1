// import React from 'react';
// import {shallow} from 'enzyme';
//
// import MoveHistory from '../../../src/components/WordGame/WordGameHistory';
// import Move from '../../../src/components/WordGame/WordGameMove';
//
// describe('Word game move history', () => {
//   it('renders no Move components without moves', () => {
//     expect(shallow(<MoveHistory moves={[]} targetWord='paper'/>))
//       .to.not.contain.descendants(Move);
//   });
//
//   it('renders Move component for each move', () => {
//     const moves = [
//       {comparedToAnswer: 'WR', guess: 'grill'},
//       {comparedToAnswer: 'EQ', guess: 'paper'}
//     ];
//
//     const moveHistory = shallow(<MoveHistory moves={moves} targetWord='paper'/>);
//
//     expect(moveHistory).to.have.exactly(2).descendants(Move);
//     expect(moveHistory).to.contain(<Move comparedToAnswer="WR" guess='grill' targetWord='paper'/>);
//     expect(moveHistory).to.contain(<Move comparedToAnswer="EQ" guess='paper' targetWord='paper'/>);
//   });
// });
