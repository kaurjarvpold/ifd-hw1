import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';

import OnlinePlayers from '../../src/components/OnlinePlayers';

describe('<OnlinePlayers>', function () {
  it('renders no players without any connected players', function () {
    const wrapper = shallow(<OnlinePlayers players={[]} />);
    expect(wrapper.find('div')).to.have.length(1);
  });

  it('renders player names when players are connected', function () {
    const wrapper = shallow(<OnlinePlayers players={[{id: '111', name: 'Peeter'}]} />);
    expect(wrapper).to.contain.text('Peeter');
  });

  it('should have props for players and current player', function () {
    const wrapper = shallow(<OnlinePlayers players={[]} />);
    expect(wrapper.props().players).to.be.defined;
    expect(wrapper.props().currentPlayer).to.be.defined;
  });
});
