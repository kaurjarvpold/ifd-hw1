import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';

import ConnectToServer from '../../src/components/ConnectToServer';

describe('<ConnectToServer>', function () {
  it('should have an input for the name', function () {
    const wrapper = shallow(<ConnectToServer/>);
    expect(wrapper.find('input')).to.have.length(1);
  });

  it('should have a button', function () {
    const wrapper = shallow(<ConnectToServer/>);
    expect(wrapper.find('button')).to.have.length(1);
  });

  it('should have props for connecting to a websocket', function () {
    const wrapper = shallow(<ConnectToServer/>);
    expect(wrapper.props().connecting).to.be.defined;
  });
});
