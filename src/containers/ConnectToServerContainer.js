import {connect} from 'react-redux';
import {connecting} from '../actions';
import ConnectToServer from '../components/ConnectToServer';

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    connecting: ({playerName}) => {
      dispatch(connecting({playerName}));
      ownProps.history.push('/createGame');
    },
  };
};

export default connect(undefined, mapDispatchToProps)(ConnectToServer);
