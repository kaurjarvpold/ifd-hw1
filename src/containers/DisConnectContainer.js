import {connect} from 'react-redux';
import DisConnect from '../components/DisConnect';

const mapDispatchToProps = (dispatch) => {
  return {
    disConnect: () => dispatch({type: 'DISCONNECT'}),
  };
};

export default connect(undefined, mapDispatchToProps)(DisConnect);
