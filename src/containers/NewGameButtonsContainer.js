import React from 'react';
import {connect} from 'react-redux';
import {newGameRequested} from '../actions';
import NewGameButtons from '../components/NewGameButtons';

const mapStateToProps = (state) => {
  return {
    games: state.games,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    newGameRequested: ({game_type}) => {
      dispatch(newGameRequested({game_type}));
      ownProps.history.push('/ongoingGames');
    },
  };
};

NewGameButtons.PropTypes = {
  games: React.PropTypes.array.isRequired,
  newGameRequested: React.PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(NewGameButtons);
