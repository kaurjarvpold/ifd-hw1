import React from 'react';
import NewGameButtonsContainer from './NewGameButtonsContainer';
import OnGoingGames from './OnGoingGames';
import {connect} from 'react-redux';
import ConnectToServerContainer from './ConnectToServerContainer';
import OnlinePlayersContainer from './OnlinePlayersContainer';
import DisConnectContainer from './DisConnectContainer';
import GameComponentContainer from './GameComponentContainer';
import {Route, Link} from 'react-router-dom';
import {ConnectedRouter} from 'connected-react-router';

const mapStateToProps = (state) => {
  return {
    status: state.connection_status,
  };
};

const App = (props) => {
  if(props.status === 'connected') {
    return (
      <ConnectedRouter history={props.history}>
        <div className="app">
          <div className="app-header">
            <h1>Game Lobby</h1>
          </div>
          <Route path="/createGame" component={NewGameButtonsContainer}/>
          <Route path="/ongoingGames" component={OnGoingGames} />
          <Route path="/finishedGames" component={OnGoingGames} />
          <Route path="/players" component={OnlinePlayersContainer} />
          <Route path="/games/:gameId" component={GameComponentContainer} />
          <Link to={`/createGame`}>New Game</Link>
          <DisConnectContainer/>
        </div>
      </ConnectedRouter>
    );
  } else if (props.status === 'connecting') {
    return (
      <div className="app">
        <div className="app-header">
          <h1>Game Lobby</h1>
        </div>
        <p> Connecting... </p>
      </div>
    );
  } else if (props.status === 'disconnected') {
    return (
      <ConnectedRouter history={props.history}>
        <div className="app">
          <div className="app-header">
            <h1>Game Lobby</h1>
          </div>
          <Route component={ConnectToServerContainer} />
        </div>
      </ConnectedRouter>
    );
  }
};

App.PropTypes = {
  status: React.PropTypes.string.isRequired,
  history: React.PropTypes.object.isRequired,
};

export default connect(mapStateToProps, undefined)(App);
