import React from 'react';
import GameComponent from '../components/GameComponent';
import {connect} from 'react-redux';
import {guessRequested} from '../actions';

const GameOrNotFound = (props) => {
  if (props.game) {
    return <GameComponent game={props.game}
                          moveInFlight={props.moveInFlight}
                          guessRequested={props.guessRequested}
            />;
  } else {
    return <p>Game {props.gameId} not found</p>;
  }
};
GameOrNotFound.propTypes = {
  game: React.PropTypes.object.isRequired,
  moveInFlight: React.PropTypes.bool.isRequired,
  guessRequested: React.PropTypes.func.isRequired,
  gameId: React.PropTypes.string.isRequired,
};

const mapDispatchToProps = (dispatch) => {
  return {
    guessRequested: ({id, guess}) => dispatch(guessRequested({id, guess})),
  };
};

const mapStateToProps = (state, ownProps) => {
  const gameId = ownProps.match.params.gameId;
  const game = state.games.find((game) => game.id === gameId);
  const moveInFlight = state.moveInFlight;
  return {game, gameId: gameId, moveInFlight: moveInFlight};
};

export default connect(mapStateToProps, mapDispatchToProps)(GameOrNotFound);
