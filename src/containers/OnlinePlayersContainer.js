import {connect} from 'react-redux';
import OnlinePlayers from '../components/OnlinePlayers';

const mapStateToProps = (state) => {
  return {
    players: state.players,
    currentPlayer: state.currentPlayer,
  };
};

export default connect(mapStateToProps, undefined)(OnlinePlayers);
