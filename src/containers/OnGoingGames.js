import React from 'react';
import {connect} from 'react-redux';
import GameList from '../components/GameList';
import {guessRequested} from '../actions';
import {
  BrowserRouter as Router,
  Link,
} from 'react-router-dom';
import {ConnectedRouter} from 'connected-react-router';

const OnGoingGames = (props) => {
  if (window.location.href == 'http://localhost:8080/ongoingGames') {
    return (
      <ConnectedRouter history={props.history}>
        <div className='ongoing-games'>
          <h2>Ongoing Games</h2>
          <GameList
            games={props.games}
            guessRequested={props.guessRequested}
            gameInFlight={props.gameInFlight}
            moveInFlight={props.moveInFlight}
          />
          <Link to={`/finishedGames`}>Finished Games</Link>
          <Link to={`/players`}>Online Players</Link>
        </div>
      </ConnectedRouter>
    );
  } else if (window.location.href == 'http://localhost:8080/finishedGames') {
    return (
      <ConnectedRouter history={props.history}>
        <div className='ongoing-games'>
          <h2>Finished Games</h2>
          <GameList
            games={props.games}
            guessRequested={props.guessRequested}
            gameInFlight={props.gameInFlight}
            moveInFlight={props.moveInFlight}
          />
          <Link to={`/ongoingGames`}>Ongoing Games</Link>
          <Link to={`/players`}>Online Players</Link>
        </div>
      </ConnectedRouter>
    );
  }
};

const mapStateToProps = (state) => {
  return {
    games: state.games,
    gameInFlight: state.gameInFlight,
    moveInFlight: state.moveInFlight,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    guessRequested: ({id, guess}) => dispatch(guessRequested({id, guess})),
  };
};

OnGoingGames.propTypes = {
  games: React.PropTypes.array.isRequired,
  guessRequested: React.PropTypes.func.isRequired,
  gameInFlight: React.PropTypes.bool.isRequired,
  moveInFlight: React.PropTypes.bool.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(OnGoingGames);
