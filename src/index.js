import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import reducer from './reducers';
import App from './containers/App';
import gameServerMiddleware from './middlewares/GameServerMiddleware';
import {createBrowserHistory} from 'history';
import {connectRouter, routerMiddleware} from 'connected-react-router';

const composeStoreEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ||
  compose;

const history = createBrowserHistory();

let store = createStore(
  connectRouter(history)(reducer),
  composeStoreEnhancers(
    applyMiddleware(
      routerMiddleware(history),
      thunk,
      gameServerMiddleware
    )
  )
);

ReactDOM.render(
  <Provider store={store}>
    <App history={history}/>
  </Provider>,
  document.getElementById('root')
);
