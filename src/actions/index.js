const createPayloadForwardingAction = (type) => (payload) =>
  ({type: type, payload: payload});

export const NEW_GAME_REQUESTED = 'NEW_GAME_REQUESTED';
export const newGameRequested = ({game_type}) => ({
  type: NEW_GAME_REQUESTED,
  payload: {
    game_type,
  },
});

export const NEW_GAME_SUCCEEDED = 'NEW_GAME_SUCCEEDED';
export const newGameSucceeded =
  createPayloadForwardingAction(NEW_GAME_SUCCEEDED);

export const NEW_GAME_FAILED = 'NEW_GAME_FAILED';
export const newGameFailed = createPayloadForwardingAction(NEW_GAME_FAILED);

export const GUESS_REQUESTED = 'GUESS_REQUESTED';
export const guessRequested = ({id, guess}) => ({
  type: GUESS_REQUESTED,
  payload: {
    id,
    guess,
  },
});

export const GUESS_SUCCEEDED = 'GUESS_SUCCEEDED';
export const guessSucceeded = createPayloadForwardingAction(GUESS_SUCCEEDED);

export const GUESS_FAILED = 'GUESS_FAILED';
export const guessFailed = createPayloadForwardingAction(GUESS_FAILED);

export const CONNECTING = 'CONNECTING';
export const connecting = ({playerName}) => ({
  type: CONNECTING,
  payload: {
    playerName,
  },
});

export const CONNECTED = 'CONNECTED';
export const connected = createPayloadForwardingAction(CONNECTED);

export const DISCONNECTED = 'DISCONNECTED';
export const disconnected = createPayloadForwardingAction(DISCONNECTED);
