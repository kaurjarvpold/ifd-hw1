import {connect as connectWebSocket} from '../WebSocket';

const messageToAction = {
  'connection:accepted': ({playerId}) => ({type: 'CONNECTION_ACCEPTED',
                                           payload: {playerId}}),
  'online-players': (payloadd) => ({type: 'ONLINE_PLAYERS_RECEIVED',
                                    payload: payloadd}),
};

let webSocketConnection = null;
export const initiateConnection = ({playerName}) => (dispatch) => {
  webSocketConnection = connectWebSocket({
    onOpen: () =>
      dispatch({type: 'CONNECTED'}),
    onClose: ({reason}) =>
      dispatch({type: 'DISCONNECTED', payload: {reason}}),
    onMessage: ({eventName, payload}) => {
      dispatch(messageToAction[eventName](payload));
    },
    parameters: {playerName: playerName},
  });
};

export const disConnect = () => (dispatch) => {
  webSocketConnection.close();
};
