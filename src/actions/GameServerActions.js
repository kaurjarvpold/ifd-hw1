import jsonAjax from '../JSONAjaxRequest';

import {
  newGameSucceeded,
  newGameFailed,
  guessSucceeded,
  guessFailed,
} from './index';

const SERVER_ADDRESS = 'http://localhost:8081';

export const newGame = ({game_type}) => (dispatch) => {
  jsonAjax(
    SERVER_ADDRESS + '/games',
    'POST',
    {type: game_type},
    ({id, type, status}) => dispatch(newGameSucceeded({id, type, status})),
    (error) => dispatch(newGameFailed(error))
  );
};

export const newGuess = ({id, guess}) => (dispatch) => {
  jsonAjax(
    'http://localhost:8081/games/' + id + '/moves',
    'POST',
    {guess: guess},
    (moveResponse) => {
      dispatch(guessSucceeded(moveResponse));
    },
    (error) => {
      dispatch(guessFailed(error));
    }
  );
};
