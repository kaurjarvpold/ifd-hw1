import React from 'react';

const Char = ({a, correct}) => {
  if (correct) {
    return (
        <span className="Green">{a}</span>
    );
  } else {
    return (
        <span className="Red">{a}</span>
    );
  }
};

Char.propTypes = {
  a: React.PropTypes.string.isRequired,
  correct: React.PropTypes.bool.isRequired,
};

export default Char;
