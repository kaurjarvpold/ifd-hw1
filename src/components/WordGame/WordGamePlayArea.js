import React from 'react';
import InputChangesOnSubmit from '../InputChangesOnSubmit';

const WordGamePlayArea = ({gameStatus, onGuess}) => {
  let PlayArea;

  if (gameStatus === 'finished') {
    PlayArea = (
      <p> You won! </p>
    );
  } else {
    PlayArea = (
      <div>
        <p> Guess a 5 letter word </p>
        <InputChangesOnSubmit onSubmit={onGuess} type='string' />
      </div>
    );
  }

  return PlayArea;
};

WordGamePlayArea.PropTypes = {
  gameStatus: React.PropTypes.string.isRequired,
  onGuess: React.PropTypes.func.isRequired,
};

export default WordGamePlayArea;
