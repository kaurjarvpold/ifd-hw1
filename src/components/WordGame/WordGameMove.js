import React from 'react';
import Char from './WordGameChar';

const Move = ({guess, correct, letterMatches}) => {
  const resultClass = correct ? 'correct' : 'incorrect';
  const guessArray = guess.split('');

  const result = guessArray.map((char, index) =>
    <Char
      a = {char}
      key = {index}
      correct = {letterMatches[index]}
    />
  );

  return (
    <div className={'move ' + resultClass}>
      {result}
    </div>
  );
};

Move.propTypes = {
  guess: React.PropTypes.string.isRequired,
  correct: React.PropTypes.bool.isRequired,
  letterMatches: React.PropTypes.array.isRequired,
};

export default Move;
