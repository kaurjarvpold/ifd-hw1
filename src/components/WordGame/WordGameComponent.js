import React, {Component} from 'react';
import MoveHistory from '../../components/WordGame/WordGameHistory';
import WordGamePlayArea from '../../components/WordGame/WordGamePlayArea';

class WordGameComponent extends Component {
  constructor(props) {
    super(props);
    this.onGuess = this.onGuess.bind(this);
  }

  onGuess(guess) {
    this.props.guessRequested({id: this.props.game.id, guess: guess});
  }

  render() {
    return (
      <div className='word-game'>
        <h3> Word Guess Game </h3>
        <WordGamePlayArea
          gameStatus={this.props.game.status}
          onGuess={this.onGuess}
        />
        <MoveHistory
          moves={this.props.game.moves}
          moveInFlight={this.props.moveInFlight}
        />
      </div>
    );
  }
}

WordGameComponent.propTypes = {
  game: React.PropTypes.shape({
    id: React.PropTypes.string.isRequired,
    type: React.PropTypes.string.isRequired,
    status: React.PropTypes.string.isRequired,
    moves: React.PropTypes.array.isRequired,
  }),
  guessRequested: React.PropTypes.func.isRequired,
  moveInFlight: React.PropTypes.bool.isRequired,
};

export default WordGameComponent;
