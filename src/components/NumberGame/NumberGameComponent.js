import React, {Component} from 'react';
import MoveHistory from '../../components/NumberGame/NumberGameHistory';
import NumberGamePlayArea from '../../components/NumberGame/NumberGamePlayArea';

class NumberGameComponent extends Component {
  constructor(props) {
    super(props);
    this.onGuess = this.onGuess.bind(this);
  }

  onGuess(guess) {
    const guessNumber = parseInt(guess);
    this.props.guessRequested({id: this.props.game.id, guess: guessNumber});
  }

  render() {
    return (
      <div className='number-game'>
        <h3> Number Guess Game </h3>
        <NumberGamePlayArea
          gameStatus={this.props.game.status}
          onGuess={this.onGuess}
        />
        <MoveHistory
          moves={this.props.game.moves}
          moveInFlight={this.props.moveInFlight}
        />
      </div>
    );
  }
}

NumberGameComponent.propTypes = {
  game: React.PropTypes.shape({
    id: React.PropTypes.string.isRequired,
    type: React.PropTypes.string.isRequired,
    status: React.PropTypes.string.isRequired,
    moves: React.PropTypes.array.isRequired,
  }),
  guessRequested: React.PropTypes.func.isRequired,
  moveInFlight: React.PropTypes.bool.isRequired,

};

export default NumberGameComponent;
