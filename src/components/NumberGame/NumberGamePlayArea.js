import React from 'react';
import InputChangesOnSubmit from '../InputChangesOnSubmit';

const NumberGamePlayArea = ({gameStatus, onGuess}) => {
  let PlayArea;

  if (gameStatus === 'finished') {
    PlayArea = (
      <p> You won! </p>
    );
  } else {
    PlayArea = (
      <div>
        <p> Guess a number from 0 to 9 </p>
        <InputChangesOnSubmit onSubmit={onGuess} type='number' />
      </div>
    );
  }

  return PlayArea;
};

NumberGamePlayArea.PropTypes = {
  gameStatus: React.PropTypes.string.isRequired,
  onGuess: React.PropTypes.func.isRequired,
};

export default NumberGamePlayArea;
