import React from 'react';

const COMPARISON_TO_DESCRIPTION = {
  EQ: 'correct',
  LT: 'smaller than target',
  GT: 'greater than target',
};

const Move = ({guess, comparedToAnswer}) => {
  const resultClass = comparedToAnswer == 'EQ' ? 'correct' : 'incorrect';

  return (
    <div className={'move-' + resultClass}>
      <span><i>{guess}</i>: was {COMPARISON_TO_DESCRIPTION[comparedToAnswer]}
       </span>
    </div>
  );
};

Move.propTypes = {
  guess: React.PropTypes.number.isRequired,
  comparedToAnswer: React.PropTypes.string.isRequired,
};

export default Move;
