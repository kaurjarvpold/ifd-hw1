import React from 'react';
import Move from './NumberGameMove';

const MoveHistory = ({moves, moveInFlight}) => {
  const moveHistory = moves.map((move, index) =>
    <Move
      guess = {move.guess}
      comparedToAnswer = {move.comparedToAnswer}
      key = {index}
    />
  );

  if (moveHistory.length === 0) {
    if (moveInFlight) {
      return (
        <div className = 'move-history'>
          <p>Processing your move...</p>
        </div>
      );
    } else {
      return (<div className = 'move-history'/>);
    }
  } else {
    if (moveInFlight) {
      return (
        <div className='move-history'>
          <p>Previous moves:</p>
          <p>Processing your move...</p>
          <div className='move-list'>
            {moveHistory}
          </div>
        </div>
      );
    } else {
      return (
        <div className='move-history'>
          <p>Previous moves:</p>
          <div className='move-list'>
            {moveHistory}
          </div>
        </div>
      );
    }
  }
};

MoveHistory.propTypes = {
  moves: React.PropTypes.array.isRequired,
  moveInFlight: React.PropTypes.bool.isRequired,
};

export default MoveHistory;
