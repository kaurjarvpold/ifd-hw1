import React, {Component} from 'react';

class ConnectToServer extends Component {
  constructor(props) {
    super(props);
    this.onKeyUp = this.onKeyUp.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onClick = this.onClick.bind(this);
    this.state = {value: ''};
  }

  onChange(event) {
    this.setState({
      value: event.target.value,
    });
  }

  onKeyUp(event) {
    if (event.keyCode === 13) {
      const submissionValue = this.state.value;
      this.setState({value: ''});
      this.props.connecting({playerName: submissionValue});
    }
  }

  onClick() {
    const submissionValue = this.state.value;
    this.setState({value: ''});
    this.props.connecting({playerName: submissionValue});
  }

  render() {
    return (
      <div>
        <span>Your name: </span>
        <input
          type={'string'}
          onKeyUp={this.onKeyUp}
          value={this.state.value}
          onChange={this.onChange}
        />
        <button className="connect-to-server" onClick={this.onClick}>
          Connect
        </button>
      </div>
    );
  }
}

ConnectToServer.propTypes = {
  connecting: React.PropTypes.func.isRequired,
};

export default ConnectToServer;
