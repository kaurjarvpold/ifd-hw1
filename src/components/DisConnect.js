import React, {Component} from 'react';

class DisConnect extends Component {
  constructor(props) {
    super(props);
    this.disConnect = this.disConnect.bind(this);
  }

  disConnect() {
    this.props.disConnect();
  }

  render() {
    return (
      <div className='disconnect'>
        <button className="disconnect-button" onClick={this.disConnect}>
          DisConnect
        </button>
      </div>
    );
  }
}

DisConnect.propTypes = {
  disConnect: React.PropTypes.func.isRequired,
};

export default DisConnect;
