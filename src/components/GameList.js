import React from 'react';
import {Link} from 'react-router-dom';

const GameList = (props) => {
  let gameStatus = '';
  const allGames = props.games.map((game, index) => {
    if (window.location.href == 'http://localhost:8080/ongoingGames') {
      gameStatus = 'waiting_for_move';
    } else if (window.location.href == 'http://localhost:8080/finishedGames') {
      gameStatus = 'finished';
    }
    if (game.status === gameStatus) {
      if (game.type === 'guess_number') {
        return (
          <div key={game.id}>
              <Link to={`/games/${game.id}`}>
                Type: {game.type} Status: {game.status}
              </Link>
          </div>
          );
        } else {
          return (
            <div key={game.id}>
              <Link to={`/games/${game.id}`}>
                Type: {game.type} Status: {game.status}
              </Link>
            </div>
          );
        }
      }
    }
  );

  if(props.gameInFlight) {
    return (
      <div className='game-list'>
        {allGames}
        <p>Game is loading...</p>
      </div>
    );
  } else {
    return (
      <div className='game-list'>
        {allGames}
      </div>
    );
  }
};

GameList.propTypes = {
  games: React.PropTypes.array.isRequired,
  gameInFlight: React.PropTypes.bool.isRequired,
};

export default GameList;
