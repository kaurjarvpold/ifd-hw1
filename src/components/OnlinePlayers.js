import React from 'react';

const OnlinePlayers = (props) => {
  const allPlayers = props.players.map((player, index) => {
      if (props.currentPlayer === player.id) {
        return (
          <div className = 'you' key = {index}>{player.name} (You)</div>
        );
      } else {
        return (
          <div className = 'player' key = {index}>{player.name}</div>
        );
      }
    }
  );

  return (
    <div className="player-list">
      <h3>Online Players:</h3>
      {allPlayers}
    </div>
  );
};

OnlinePlayers.propTypes = {
  players: React.PropTypes.array.isRequired,
  currentPlayer: React.PropTypes.string.isRequired,
};

export default OnlinePlayers;
