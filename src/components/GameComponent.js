import React from 'react';
import NumberGameComponent from './NumberGame/NumberGameComponent';
import WordGameComponent from './WordGame/WordGameComponent';
import {Link} from 'react-router-dom';

const GameComponent = (props) => {
  if (props.game.type === 'guess_number') {
    return (
        <div>
          <NumberGameComponent
            game = {props.game}
            guessRequested = {props.guessRequested}
            moveInFlight = {props.moveInFlight}
          />
          <Link to={`/ongoingGames`}>Ongoing Games</Link>
          <Link to={`/finishedGames`}>Finished Games</Link>
          <Link to={`/players`}>Online Players</Link>
        </div>
      );
    } else {
      return (
        <div>
          <WordGameComponent
            game = {props.game}
            guessRequested = {props.guessRequested}
            moveInFlight = {props.moveInFlight}
          />
          <Link to={`/ongoingGames`}>Ongoing Games</Link>
          <Link to={`/finishedGames`}>Finished Games</Link>
          <Link to={`/players`}>Online Players</Link>
        </div>
      );
    }
};

GameComponent.propTypes = {
  game: React.PropTypes.object.isRequired,
  guessRequested: React.PropTypes.func.isRequired,
  moveInFlight: React.PropTypes.bool.isRequired,
};

export default GameComponent;
