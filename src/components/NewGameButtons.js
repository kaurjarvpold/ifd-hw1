import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class NewGameButtons extends Component {
  constructor(props) {
    super(props);
    this.newNumberGame = this.newNumberGame.bind(this);
    this.newWordGame = this.newWordGame.bind(this);
  }

  newNumberGame() {
    this.props.newGameRequested({game_type: 'guess_number'});
  }

  newWordGame() {
    this.props.newGameRequested({game_type: 'guess_word'});
  }

  render() {
    return (
      <div>
        <div className='new-game-buttons'>
          <button className="new-number-game" onClick={this.newNumberGame}>
            New Number Game
          </button>
          <button className="new-word-game" onClick={this.newWordGame}>
            New Word Game
          </button>
        </div>
        <Link to={`/players`}>Online Players</Link>
      </div>
    );
  }
}

NewGameButtons.propTypes = {
  newGameRequested: React.PropTypes.func.isRequired,
};

export default NewGameButtons;
