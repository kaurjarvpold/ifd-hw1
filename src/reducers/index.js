import {
  NEW_GAME_SUCCEEDED,
  NEW_GAME_FAILED,
  NEW_GAME_REQUESTED,
  GUESS_REQUESTED,
  GUESS_SUCCEEDED,
  GUESS_FAILED,
  CONNECTING,
  CONNECTED,
} from '../actions/index';

const initialGames = [];
const initalPlayers = [];
const initialState = {
  gameInFlight: false,
  moveInFlight: false,
  connection_status: 'disconnected',
  games: initialGames,
  players: initalPlayers,
  currentPlayer: '',
};

const gameReducer = (state = initialState, action) => {
  switch (action.type) {
    case NEW_GAME_REQUESTED: {
      return {...state, gameInFlight: true};
    }
    case NEW_GAME_SUCCEEDED: {
      const newGames = state.games.concat({
        id: action.payload.id,
        type: action.payload.type,
        status: action.payload.status,
        moves: [],
      });
      return {...state, gameInFlight: false, games: newGames};
    }
    case NEW_GAME_FAILED: {
      alert('Creating the game failed. Please try again!');
      return {...state, gameInFlight: false};
    }
    case GUESS_REQUESTED: {
      return {...state, moveInFlight: true};
    }
    case GUESS_SUCCEEDED: {
      const gameId = action.payload.game.id;
      const move = action.payload.move;
      function findGame(game) {
          return game.id === gameId;
      }
      const gameIndex = state.games.findIndex(findGame);
      const getNewGame = () => {
        const game = state.games.find(findGame);
        const newMoves = game.moves.concat(move);
        const newGame = {...game,
          moves: newMoves,
          status: action.payload.game.status,
        };
        return newGame;
      };
      const newGame = getNewGame();
      const newGames = [
        ...state.games.slice(0, gameIndex),
        newGame,
        ...state.games.slice(gameIndex + 1),
      ];
      return {...state, moveInFlight: false, games: newGames};
    }
    case GUESS_FAILED: {
      alert('Failed to process move. Please try again.');
      return {...state, moveInFlight: false};
    }
    case CONNECTING: {
      return {...state, connection_status: 'connecting'};
    }
    case CONNECTED: {
      return {...state, connection_status: 'connected'};
    }
    case 'ONLINE_PLAYERS_RECEIVED': {
      return {...state, players: action.payload};
    }
    case 'CONNECTION_ACCEPTED': {
      return {...state, currentPlayer: action.payload.playerId};
    }
    case 'DISCONNECTED': {
      return {...state, connection_status: 'disconnected',
                        players: initalPlayers,
                        currentPlayer: '',
                        games: initialGames};
    }
    default:
      return state;
  }
};

export default gameReducer;
