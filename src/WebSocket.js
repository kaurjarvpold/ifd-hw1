const websocketConf = {
  port: 8081,
  host: 'localhost',
};

const objectToQueryString = (obj) => {
  let queryParts = [];
  for(const p in obj)
    if (obj.hasOwnProperty(p)) {
      queryParts.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
    }
  return queryParts.join('&');
};

export const connect = ({onOpen, onClose, onMessage, parameters = {}}) => {
  const websocketConnection = new WebSocket(
    `ws://${websocketConf.host}:${websocketConf.port}/?${objectToQueryString(parameters)}`
  );

  websocketConnection.onopen = () => onOpen();

  websocketConnection.onclose = (event) => {
    const reason = event.reason;
    onClose({reason});
  };

  websocketConnection.onmessage = (messageEvent) => {
    const payload = messageEvent.data;

    let parsedMessage;
    try {
      parsedMessage = JSON.parse(payload);
    } catch (error) {
      console.error('error parsing websocket message', error, payload);
      return;
    }
    onMessage(parsedMessage);
  };

  const close = () => {
    websocketConnection.close();
  };

  return {
    close: close,
  };
};
