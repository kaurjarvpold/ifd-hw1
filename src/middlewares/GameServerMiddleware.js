import {
  NEW_GAME_REQUESTED,
  GUESS_REQUESTED,
  CONNECTING,
} from '../actions/index';

import {
  newGame,
  newGuess,
} from '../actions/GameServerActions';

import {
  initiateConnection,
  disConnect,
} from '../actions/WebSocketActions';

const ACTION_TYPE_TO_SERVER_ACTION = {
  [NEW_GAME_REQUESTED]: newGame,
  [GUESS_REQUESTED]: newGuess,
  [CONNECTING]: initiateConnection,
  ['DISCONNECT']: disConnect,
};

const gameServerMiddleware = (store) => (next) => (action) => {
  const serverAction = ACTION_TYPE_TO_SERVER_ACTION[action.type];
  if (serverAction) {
    serverAction(action.payload)(store.dispatch);
  }
  return next(action);
};

export default gameServerMiddleware;
