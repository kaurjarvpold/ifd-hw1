var path = require('path');

module.exports = {
  entry: [
    './src/index.js'
  ],
  devServer: {
    port: 8080,
    historyApiFallback: true
  },
  devtool: "inline-source-map",
  output: {
    filename: 'dist/bundle.js',
    path: path.join(__dirname, '/'),
  },
  module: {
    rules: [
      { test: /\.js/, use: 'babel-loader' }
    ]
  }
};
