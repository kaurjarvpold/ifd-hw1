module.exports = {
    "parser": "babel-eslint",
    "parserOptions": {
      "ecmaFeatures": {
        "jsx": true
      }
    },
    "env": {
        "browser": true,
        "mocha": true,
        "jasmine": true,
        "node": true
    },
    "plugins": ["react", "import"],
    "extends": [
      "eslint:recommended",
      "plugin:react/recommended",
      "google",
      "plugin:import/errors",
      "plugin:import/warnings"
    ],
    "rules": {
        "linebreak-style": [
            "error",
            "windows"
        ],
        "semi": [
            "error",
            "always"
        ]
    }
};
